import Vue from 'vue'
import VueHead from 'vue-head'
import router from './router'
import axios from 'axios'
import VueAxios from 'vue-axios'
import App from './App'

// import store from './store'

Vue.config.productionTip = false

Vue.use(VueHead)
Vue.use(VueAxios, axios)

new Vue({
  render: h => h(App),
  router,
  // store,
  template: '',
}).$mount('#app')
