const express = require('express')
const cors = require('cors')
const app = express()
const middleware = require('./middleware/index.js')
const getRouter = require('./router/index.js')
const server = () => {
  app.listen(8000, () => {
    console.log('Server started')
  })
}

middleware(app)
getRouter().then(router => {
  app.use(cors())
  app.use(router)
  server()
})
