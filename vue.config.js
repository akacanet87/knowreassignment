const path = require('path')
const webpack = require('webpack')

module.exports = {
  runtimeCompiler: true,
  chainWebpack: config => {
    config.resolve.symlinks(true)
    if (config.plugins.has('extract-css')) {
      const extractCSSPlugin = config.plugin('extract-css')
      extractCSSPlugin && extractCSSPlugin.tap(() => [{
        filename: '[name].css',
        chunkFilename: '[name].css',
      }])
    }
  },
  devServer: {
    proxy: 'http://localhost:8000/',
  },
  configureWebpack: {
    output: {
      filename: '[name].js',
      chunkFilename: '[name].js',
    },
    resolve: {
      extensions: ['*', '.js', '.vue', '.json'],
      alias: {
        'vue$': 'vue/dist/vue.runtime.esm.js',
        '@': path.resolve(__dirname, './'),
      },
    },
    plugins: [
      new webpack.ProvidePlugin({
        $: 'jquery',
        jquery: 'jquery',
        'window.jQuery': 'jquery',
        jQuery: 'jquery',
      }),
    ],
  },
  css: {
    loaderOptions: {
      sass: {
        data: `
          @import "@/src/assets/styles/variables.scss";
          @import "@/src/assets/styles/commons.scss";
        `,
      },
    },
  },
}
